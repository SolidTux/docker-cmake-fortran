# CMake Fortran Docker image

Docker image based on Fedora with CMake, gfortran and LaTeX for use in Gitlab runners.

## Setup

### Prerequisites

The first requirement is a machine with Docker installed (instructions can be found [here](https://docs.docker.com/install/)).
While Windows could work (but the CI config for this project might have to be adjusted), the setup is only tested under Linux.
Additionally the Gitlab runner service has to be setup like described [here](https://docs.gitlab.com/runner/install/).

Next, add the `gitlab-runner` user has to be added to the `docker` group, as the shell executor is used to build the Docker image
([here](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html) is a more detailed description about that topic):

```
sudo gpasswd -a gitlab-runner docker
```

### Registering the Runners

For every project, the runner token is needed, which can be found in the settings under CI.

The runner for projects, which need to build Docker images (like this one), can be registered by using
(`TOKEN` is the already mentioned runner token and `NAME` can be freely chosen):

```
sudo gitlab-runner register --non-interactive --name NAME --url https://git.scc.kit.edu --registration-token TOKEN --executor shell
```

For other projects using this or other local images the following command is needed:

```
sudo gitlab-runner register --non-interactive --name NAME --url https://git.scc.kit.edu --registration-token TOKEN --executor docker --docker-image IMAGE --docker-pull-policy if-not-present
```

`IMAGE` is here the default Docker image, which is used when no image is given. This can be set for example to `cmake-fortran`
for this image. As most configurations probably set an image, this is not really important.

The important part, which differs here from the usual setup, is the `if-not-present` policy.
By default the Gitlab runner only searches online for images. This setting first looks locally for the image
and only looks online, if the image is not found. This is necessary to be able to use this locally build image here.