FROM fedora
RUN dnf install -y @development-tools
RUN dnf install -y cmake openmpi openmpi-devel python3-numpy python3-matplotlib make environment-modules doxygen texlive-scheme-full graphviz latexmk lapack-devel biber
ARG CACHEBUST=1
RUN dnf upgrade --refresh -y
RUN useradd gitlab
RUN echo "source /usr/share/Modules/init/bash" >> /etc/profile
RUN echo "module load mpi" >> /etc/profile
ENV BASH_ENV="/etc/profile"
USER gitlab
